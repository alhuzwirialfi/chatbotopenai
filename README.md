### Chatbot API

ChatbotApi adalah sebuah aplikasi chatbot yang dibangun menggunakan Java Spring Boot Framework. Layanan chatbot diambil dari OpenAI GPT-3.5-Turbo.
aplikasi ini terdiri dari dua file Framework aplikasi :
- Back end sebagai restApi -> mengunakan Java Springboot
- Front end sebagai tampilan UI -> menggunakan AngularJs
  - untuk file Front end bis dikunjungi disini <a href="https://github.com/Alhuzsyam/chatbotFe">FE Repo</a>
- untuk database nya sendiri mengunakan MySQL yang saya setup mengunakan docker 

#### Persyaratan Instalasi

Untuk menggunakan aplikasi ini, pastikan perangkat Anda memenuhi persyaratan berikut:

- Java 17
- Maven 3.8.6

#### Langkah-langkah Instalasi

Berikut adalah langkah-langkah untuk menginstal dan menjalankan aplikasi ini:

1. **Clone Projek:**
   Clone projek ini dari repository.

2. **Menyiapkan Docker Container MySQL:**
   Gunakan Docker Compose untuk menyiapkan container MySQL:
   ```bash
   docker-compose up --build


**Getting started**

**Api End point**

### Endpoints API

- **POST `/api/bot/chat`**
  - Deskripsi: Digunakan untuk menjalankan prompt pada OpenAI.
  - URL: `http://localhost:5173/api/bot/chat?prompt=a`

- **GET `/api/users/add`**
  - Deskripsi: Digunakan untuk menambahkan pengguna.
  - URL: `http://localhost:5173/api/users/add`
  - Body:
    ```json
    {
      "name": "alfi",
      "email": "alhuzwiri@gmail.co.id",
      "password": 12345
    }
    ```

- **POST `/api/users/login`**
  - Deskripsi: Digunakan untuk login pengguna.
  - URL: `http://localhost:5173/api/users/login`
  - Body:
    ```json
    {
      "email": "alhuzwiri@gmail.co.id",
      "password": "12345"
    }
    ```

- **POST `/api/prompt/add`**
  - Deskripsi: Digunakan untuk menyimpan hasil prompt.
  - URL: `http://localhost:5173/api/prompt/add`
  - Body:
    ```json
    {
      "userId": 2,
      "prompt": "halo221",
      "result": "ajsfgjsfgjsfssssssss"
    }
    ```

- **GET `/api/prompt/{userId}`**
  - Deskripsi: Digunakan untuk mengambil daftar prompt yang telah disimpan.
  - URL: `http://localhost:5173/api/prompt/2`

- **GET `/api/prompt/delete/{promptId}`**
  - Deskripsi: Digunakan untuk menghapus prompt berdasarkan ID.
  - URL: `http://localhost:5173/api/prompt/delete/7`



**Easy Use**
jika ingin mengunakan live server bisa langsung mencoba dengan mengganti localhost dengan ip berikut : **128.199.177.206:5173**

Jika ingin mencoba aplikasi secara langsung [Live Demo aplikasi](http://128.199.177.206:5050/).

![Mar-23-2024 12-31-54](https://github.com/Alhuzsyam/chatbotFe/assets/64511435/21ef7be4-90f2-46f8-a00c-9d547d5a0107)
